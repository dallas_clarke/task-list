import React, { useRef } from 'react'

interface Props {
    todo: string;
    setTodo: React.Dispatch<React.SetStateAction<string>>;
    handleAdd: (e: React.FormEvent) => void;
}

const InputField: React.FC<Props> = ({ todo, setTodo, handleAdd }) => {
  const inputRef = useRef<HTMLInputElement>(null);

  return (
    <form onSubmit={(e) => {
      handleAdd(e);
      inputRef.current?.blur();
    }}>
        <input 
            className='input__box'
            ref={inputRef}
            type="input" 
            value={todo}
            onChange={(e) => setTodo(e.target.value)}
            placeholder='Enter a task' 
        />
        <button className='input__submit'>Go</button>
    </form>
  )
}

export default InputField